package http

import (
	"fmt"
	"math"

	"github.com/gin-gonic/gin"
)

const contextCodesCount = "availableCodesCount"

func (env *Backend) AvailableCodesCount() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer c.Next()

		totalCodes := env.TotalCodes()
		usedCodes, err := env.OTPRepo.Count()
		if err != nil {
			getLogger(c).Error(err)
			return
		}

		getLogger(c).Infof("CODES AVAILABLE %d", totalCodes-usedCodes)
		c.Set(contextCodesCount, totalCodes-usedCodes)
	}
}

func (env *Backend) codesCount() uint64 {
	env.m.RLock()
	defer env.m.RUnlock()

	return env.totalCodesCount
}

func (env *Backend) setCodesCount(cc uint64) {
	env.m.Lock()
	defer env.m.Unlock()

	env.totalCodesCount = cc
}

func (env *Backend) TotalCodes() uint64 {
	if cc := env.codesCount(); cc > 0 {
		fmt.Println("1 CODES_COUNT", cc)
		return cc
	}

	fmt.Printf("%+v", *env.BackConfig)
	cc := math.Pow(float64(len(env.KeyItems)), float64(env.KeyLength))
	fmt.Println("CODES_COUNT", cc)
	env.setCodesCount(uint64(cc))
	return uint64(cc)
}
