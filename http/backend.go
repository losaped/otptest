package http

import (
	"net/http"
	"sync"

	"bitbucket.org/losaped/otptest/domain"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type BackConfig struct {
	KeyLength uint64 `envconfig:"KEY_LENGTH" default:"4"`
	KeyItems  string `envconfig:"KEY_ITEMS"`
}

// Backend providing access to various system services
type Backend struct {
	*BackConfig
	OTPRepo   domain.OTPRepository
	Generator domain.KeyGenerator

	m               sync.RWMutex
	totalCodesCount uint64
}

func getLogger(c *gin.Context) *logrus.Entry {
	return logrus.WithFields(logrus.Fields{"handler": c.HandlerName()})
}

func (env *Backend) getKey(c *gin.Context) {
	countVal, _ := c.Get(contextCodesCount)
	count, _ := countVal.(uint64)
	if count == 0 {
		c.JSON(http.StatusInternalServerError, nil)
		getLogger(c).Error("Codes count = 0")
		return
	}

	counter := 0
	for {
		counter++
		// fmt.Println("counter", counter)
		key := env.Generator.NewKey(int(env.KeyLength))

		// fmt.Println("generated key", key)
		err := env.OTPRepo.Create(key)
		if err == nil {
			c.JSON(http.StatusOK, gin.H{"key": key})
			return
		}

		if errors.Cause(err) == domain.ErrKeyAlreadyExists {
			continue
		}

		c.JSON(http.StatusInternalServerError, nil)
		getLogger(c).Error(err)
		return
	}
}

type keyReq struct {
	// ClientID string `form:"client_id" json:"client_id" binding:"required"`
	Key string `form:"key" json:"key" binding:"required"`
}

func (env *Backend) activateKey(c *gin.Context) {
	var req keyReq
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := env.OTPRepo.Activate(req.Key); err != nil {
		switch err {
		case domain.ErrKeyNotExists:
			c.JSON(http.StatusNotFound, nil)
			break
		case domain.ErrKeyActivated:
			c.JSON(http.StatusBadRequest, nil)
			break
		default:
			c.JSON(http.StatusInternalServerError, nil)
			getLogger(c).Error(err)
			break
		}

		return
	}

	c.JSON(http.StatusOK, nil)
}

func (env *Backend) keyStatus(c *gin.Context) {
	key := c.Param("key")
	if len(key) != 4 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "wrong key"})
		return
	}

	state, err := env.OTPRepo.State(key)
	if err != nil {
		c.JSON(http.StatusInternalServerError, nil)
		getLogger(c).Error(err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": state.String()})
}

func (env *Backend) availableKeys(c *gin.Context) {
	count, _ := c.Get(contextCodesCount)
	c.JSON(http.StatusOK, gin.H{"keys_count": count})
}
