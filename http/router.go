package http

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// ServerConfig struct for external config values
type ServerConfig struct {
	Addr string `envconfig:"ADDRESS"`
}

// CreateAPI init new router
func CreateAPI(env *Backend, config *ServerConfig) http.Server {
	r := gin.Default()
	r.Use(env.AvailableCodesCount())

	r.GET("/get_key", env.getKey)
	r.POST("/activate", env.activateKey)
	r.GET("/status/:key", env.keyStatus)
	r.GET("/count", env.availableKeys)

	return http.Server{
		Addr:    config.Addr,
		Handler: r,
	}
}
