package main

import (
	"context"
	ht "net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/losaped/otptest/http"
	"bitbucket.org/losaped/otptest/random"
	"bitbucket.org/losaped/otptest/redis"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

func main() {

	log := logrus.New()
	redisCnf, err := redisConfig()
	if err != nil {
		log.Fatal(err)
	}

	serverCnf, err := serverConfig()
	if err != nil {
		log.Fatal(err)
	}

	backCnf, err := backendConfig()
	if err != nil {
		log.Fatal(err)
	}

	env := &http.Backend{
		BackConfig: backCnf,
		OTPRepo:    redis.Create(redisCnf),
		Generator:  random.KeyGenerator{KeyItems: backCnf.KeyItems},
	}

	srv := http.CreateAPI(env, serverCnf)
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != ht.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown: ", err)
	}

	log.Println("Server stopped")
}

func redisConfig() (*redis.Config, error) {
	config := &redis.Config{}
	return config, errors.Wrap(envconfig.Process("REDIS", config), "parse redis config")
}

func serverConfig() (*http.ServerConfig, error) {
	config := &http.ServerConfig{}
	return config, errors.Wrap(envconfig.Process("SERVER", config), "parsing server config")
}

func backendConfig() (*http.BackConfig, error) {
	config := &http.BackConfig{}
	return config, errors.Wrap(envconfig.Process("BACK", config), "parsing backend config")
}
