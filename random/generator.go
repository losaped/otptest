package random

import (
	"math/rand"
	"time"
)

type KeyGenerator struct {
	KeyItems string
}

func (gen KeyGenerator) NewKey(keyLength int) string {
	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)

	res := make([]byte, keyLength)
	for i := 0; i < keyLength; i++ {
		pos := r.Intn(len(gen.KeyItems))
		res[i] = gen.KeyItems[pos]
	}

	return string(res)
}
