# OTPTest

## API

- GET /get_key
- POST /activate ```Key `form:"key" json:"key" binding:"required"` ```
- GET /status/:key
- GET /count

Как хранилище выбран redis, это позволит хранить все ключи в памяти, обеспечив быстрый доступ.

## Запуск

docker-compose up --build
сервис запустится на 3000 порту