module bitbucket.org/losaped/otptest

go 1.12

require (
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/gomodule/redigo v1.7.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/pkg/errors v0.8.1
	github.com/rafaeljusto/redigomock v0.0.0-20190202135759-257e089e14a1
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/ugorji/go v1.1.4 // indirect
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
