FROM golang:latest as builder

RUN mkdir /otptest

WORKDIR /otptest

COPY . .

WORKDIR /otptest/cmd
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .

FROM alpine:latest

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /otptest/cmd .

ENTRYPOINT ["./cmd"]

CMD ["./cmd"]