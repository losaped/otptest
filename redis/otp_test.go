package redis

import (
	"testing"

	"bitbucket.org/losaped/otptest/domain"
	"github.com/gomodule/redigo/redis"
	mock "github.com/rafaeljusto/redigomock"
)

func TestCreate(t *testing.T) {
	repo := &OTPRepo{}
	c := mock.NewConn()
	otp := "aaaa"
	c.Command("hget", colName, otp).ExpectError(redis.ErrNil)
	c.Command("hset", colName, otp, domain.OTPStateIssued).Expect("OK!")
	if err := repo.create(c, otp); err != nil {
		t.Error("unexpected error", err)
	}
}

func TestCreateAlreadyExists(t *testing.T) {
	repo := &OTPRepo{}
	c := mock.NewConn()
	otp := "aaaa"
	c.Command("hget", colName, otp).Expect(0)

	err := repo.create(c, otp)
	if err == nil {
		t.Error("no error, error expected")
	}
	if err != nil {
		if err != domain.ErrKeyAlreadyExists {
			t.Error("expected error ErrKeyAlreadyExists")
		}
	}
}
