package redis

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/losaped/otptest/domain"
	"github.com/gomodule/redigo/redis"
)

const colName = "otpKeys"

// Config redis options
type Config struct {
	MaxIdle     int           `envconfig:"MAX_IDLE" default:"3"`
	IdleTimeout time.Duration `envconfig:"IDLE_TIMEOUT"`
	Address     string        `envconfig:"ADDRESS"`
}

// OTPRepo implementation of domain.OTPRepo
type OTPRepo struct {
	pool     *redis.Pool
	createOp sync.Mutex
	updateOp sync.Mutex
}

// Create init new OTPRepo
func Create(config *Config) *OTPRepo {
	return &OTPRepo{
		pool: newPool(config),
	}
}

func newPool(config *Config) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     config.MaxIdle,
		IdleTimeout: config.IdleTimeout * time.Second,
		// Dial or DialContext must be set. When both are set, DialContext takes precedence over Dial.
		Dial: func() (redis.Conn, error) { return redis.Dial("tcp", config.Address) },
	}
}

// Create save new otp if not exists
func (repo *OTPRepo) Create(otp string) error {
	return repo.exec(otp, repo.create)
}

// Activate changes status of otp to Redeemed
func (repo *OTPRepo) Activate(otp string) error {
	return repo.exec(otp, repo.activate)
}

// State returns state of otp
func (repo *OTPRepo) State(otp string) (state domain.OTPState, err error) {

	err = repo.exec(otp, func(c redis.Conn, key string) error {
		val, err := redis.Int(c.Do("hget", colName, otp))
		if err != nil {
			if err == redis.ErrNil {
				state = domain.OTPStateNotIssued
				return nil
			}
			return err
		}

		state = domain.OTPState(val)
		return nil
	})

	return
}

// Count return count of issued keys
func (repo *OTPRepo) Count() (count uint64, err error) {
	err = repo.exec("", func(c redis.Conn, key string) error {
		cnt, err := redis.Int(c.Do("hlen", colName))
		fmt.Println("redis count", cnt)
		if err != nil {
			return err
		}

		count = uint64(cnt)
		return nil
	})
	return
}

func (repo *OTPRepo) exec(key string, f func(con redis.Conn, key string) error) error {
	c := repo.pool.Get()
	defer c.Close()
	return f(c, key)
}

func (repo *OTPRepo) create(c redis.Conn, otp string) error {
	repo.createOp.Lock()
	defer repo.createOp.Unlock()

	val, err := c.Do("hget", colName, otp)
	if val != nil {
		return domain.ErrKeyAlreadyExists
	}

	if err != nil && err != redis.ErrNil {
		return err
	}

	_, err = c.Do("hset", colName, otp, int(domain.OTPStateIssued))
	return err
}

func (repo *OTPRepo) activate(c redis.Conn, otp string) error {
	repo.updateOp.Lock()
	defer repo.updateOp.Unlock()

	state, err := redis.Int(c.Do("hget", colName, otp))
	if err != nil {
		if err == redis.ErrNil {
			return domain.ErrKeyNotExists
		}
		return err
	}

	if s := domain.OTPState(state); s != domain.OTPStateIssued {
		return domain.ErrKeyActivated
	}

	_, err = c.Do("hset", colName, otp, int(domain.OTPStateRedeemed))
	return err
}
