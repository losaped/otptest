package domain

// KeyGenerator abstraction for generating otp keys
type KeyGenerator interface {
	NewKey(len int) string
}
