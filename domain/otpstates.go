package domain

type OTPState int

const (
	OTPStateIssued    OTPState = 0
	OTPStateNotIssued OTPState = 1
	OTPStateRedeemed  OTPState = 2
)

func (state OTPState) String() string {
	names := [...]string{
		"Issued",
		"NotIssued",
		"Redeemed",
	}

	if state < OTPStateIssued || state > OTPStateRedeemed {
		return "Unknown"
	}

	return names[state]
}
