package domain

import "errors"

var ErrKeyAlreadyExists = errors.New("key already exists")
var ErrKeyNotExists = errors.New("key is not exists")
var ErrKeyActivated = errors.New("key already activated")

// OTPRepository interface that allow work with one time password store
type OTPRepository interface {
	Create(otp string) (err error)
	Activate(otp string) error
	State(otp string) (state OTPState, err error)
	Count() (count uint64, err error)
}
